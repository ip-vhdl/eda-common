library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- pragma translate_off
library osvvm;
context osvvm.OsvvmContext;
-- pragma translate_on

package edacom_pkg is

  -- pragma translate_off
  procedure edacom_SetLogLevel (constant g_osvvm_loglevel : in string);
  -- pragma translate_on

  function edacom_onehot_to_integer_pos (constant A : in std_logic_vector) return integer;

  function edacom_onehot_to_integer_neg (constant A : in std_logic_vector) return integer;

  type t_fifo_rd_if is record
    data  : std_logic_vector;
    empty : std_logic;
    rd    : std_logic;
    clk   : std_logic;
  end record t_fifo_rd_if;

  type t_fifo_rd_if_array is array(natural range <>) of t_fifo_rd_if;

end package edacom_pkg;

package body edacom_pkg is

  -- purpose: description

  -- pragma translate_off
  procedure edacom_SetLogLevel (
    constant g_osvvm_loglevel : in string) is
  begin

    case g_osvvm_loglevel is

      when "_debug_" =>
        setlogenable(debug, true);
        setlogenable(info, true);
        setlogenable(final, true);
        setlogenable(passed, true);
      when "_info__" =>
        setlogenable(debug, false);
        setlogenable(info, true);
        setlogenable(final, true);
        setlogenable(passed, true);
      when others =>
        setlogenable(debug, false);
        setlogenable(info, false);
        setlogenable(final, true);
        setlogenable(passed, true);

    end case;

  end procedure edacom_SetLogLevel;
  -- pragma translate_on

  function edacom_onehot_to_integer_pos (constant A : in std_logic_vector) return integer is
  begin
    for i in A'range loop

      if (A(i) = '1') then
        return i;
      end if;

    end loop;
    return - 1;
  end function edacom_onehot_to_integer_pos;

  function edacom_onehot_to_integer_neg (constant A : in std_logic_vector) return integer is
  begin
    for i in A'range loop

      if (A(i) = '0') then
        return i;
      end if;

    end loop;
    return - 1;
  end function edacom_onehot_to_integer_neg;

end package body edacom_pkg;

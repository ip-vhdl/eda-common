#! /usr/bin/env bash

# * Setup scripts

# source code directory

if [ $# == 1 ]; then
    SRCDIR=$1
else
    # download source code
    SRCDIR=/opt/HDL/osvvm/OsvvmLibraries
    [ -e $SRCDIR ] && rm -rf $SRCDIR
    git clone --recursive https://github.com/OSVVM/OsvvmLibraries.git $SRCDIR

fi

# output directory

OUTDIR=/opt/HDL/osvvm/CompiledLibraries
[ -e $OUTDIR ] && rm -rf $OUTDIR

# create temporary directory

tmp_dir=$(mktemp -d -t XXXXXXXXXX)
[ -e $tmp_dir ] && rm -rf $tmp_dir
mkdir -p $tmp_dir/scripts

# copy scripts

cp *.sh $tmp_dir/scripts/.
cp /usr/lib/ghdl/ansi_color.sh $tmp_dir/.
cp /usr/lib/ghdl/vendors/* $tmp_dir/scripts/.

# * compile

# ** OsvvmLibraries osvvm

# use ghdl provided default script
$tmp_dir/scripts/compile-osvvm.sh \
	--all \
	--output $OUTDIR \
	--source $SRCDIR/osvvm \
	--ghdl /usr/bin/ghdl

# ** OsvvmLibraries common

# use customized, local script
$tmp_dir/scripts/compile-osvvm-common.sh \
	--all \
	--output $OUTDIR \
	--source $SRCDIR/Common \
	--ghdl /usr/bin/ghdl

# ** OsvvmLibraries uart

# use customized, local script
$tmp_dir/scripts/compile-osvvm-uart.sh \
	--all \
 --output $OUTDIR \
 --source $SRCDIR/UART \
 --ghdl /usr/bin/ghdl

# ** OsvvmLibraries axi4

# use customized, local script
$tmp_dir/scripts/compile-osvvm-axi4.sh \
	--all \
 --output $OUTDIR \
 --source $SRCDIR/AXI4 \
 --ghdl /usr/bin/ghdl

# * Cleanup

[ -e tmp_dir ] && rm -rf tmp_dir

# sudo chmod -R g+w $OUTDIR
# sudo chown -R root:ghdl $OUTDIR

"""
Bla.
"""

from cocotb.result import TestError
import cocotb
from cocotb.binary import BinaryValue

from cocotb.drivers import BusDriver
from cocotb.triggers import RisingEdge, Lock, Timer, FallingEdge, with_timeout, Lock, ClockCycles
from cocotb.result import SimTimeoutError, SimFailure, TestError, TestSuccess


class WriteTransactionException(Exception):
    """
    Custom exception signaling write transactions issues.
    """

    def __init__(self, message):
        self.message = message


class ReadTransactionException(Exception):
    """
    Custom exception signaling read transactions issues.
    """

    def __init__(self, message):
        self.message = message


# * VME bus class

class vme(BusDriver):
    """On-chip peripheral bus master."""

    _signals = ["data_in", "data_out", "wr", "rd", "address"]
    _optional_signals = []

    def __init__(self, entity, name, clock, **kwargs):

        BusDriver.__init__(self, entity, name, clock, **kwargs)

        self._idle_address = 0xFFFFFC
        self._idle_data = BinaryValue('zzzzzzzz')
        self._wr_cycle_length = 3

        self._lock = Lock()
        self.log.debug("VME bus created")

    # ** Low level write

    async def _idle(self, sync=True):
        """
        Brings the bus to its idle, steady state.

        Args:
            sync:    wait for rising edge on clock initially
                     defaults to True

        Returns:
            0 when transaction is ok
        """

        # Apply values for next clock edge
        if sync:
            await RisingEdge(self.clock)

        await self._lock.acquire()

        try:
            self.bus.wr <= 0
            self.bus.rd <= 0
            self.bus.data_in <= self._idle_data
            self.bus.address <= self._idle_address
        finally:
            self._lock.release()

    async def _write(self, address, data, sync=True):
        """
        Implements a low level vme write transaction.

        Args:
            address: the address to write to
            data:    the data to write
            sync:    Wait for rising edge on clock initially.
                     defaults to True.

        Returns:
            0 when transaction is ok
        """

        # Apply values for next clock edge
        if sync:
            await RisingEdge(self.clock)

        await self._lock.acquire()

        try:
            self.bus.address <= address
            self.bus.data_in <= data
            self.bus.wr <= 1
            await ClockCycles(self.clock, self._wr_cycle_length)
        finally:
            self._lock.release()

        await self._idle()
        return 0

    # ** Low level read

    async def _read(self, address, sync=True):
        """
        Implements a low level vme read transaction.

        Args:
            address: the address to write to
            sync:    wait for rising edge on clock initially.
                     defaults to True.

        Returns:
            The read data value
        """

        # Apply values for next clock edge
        if sync:
            await RisingEdge(self.clock)

        await self._lock.acquire()

        try:
            self.bus.rd <= 1
            self.bus.address <= address
            await ClockCycles(self.clock, self._wr_cycle_length)
            returnvalue = self.bus.data_out.value
        finally:
            self._lock.release()

        await self._idle()
        return returnvalue

    # ** Write command

    async def write(self, address, data, check=True):
        """
        Implements a write transaction.

        Args:
            address: the address to write to
            data:    the data to write
            check:   performs a read too, verifying the write
                     defaults to True.

        Returns:
            0 when transaction is ok

        Raises:
            WriteTransactionException: when check fails

        """

        try:
            await self._write(address, data)
            if check:
                DataRead = await self._read(address)
                if data != DataRead:
                    raise WriteTransactionException
                    self.dut._log.warning("Wrong command: sent is {},\tread is {}".
                                          format(data, DataRead))
        except ValueError as e:
            self.log.error("exception ValueError: %s" % (e))
            raise TestError
        except WriteTransactionException as e:
            self.log.error("exception WriteTransactionException: %s" % (e))
            raise TestError

    # # ** Read command

    async def read(self, address):
        """
        Implements a read transaction.

        Args:
            address: the address to read from

        Returns:
            0 when transaction is ok
        """

        try:
            DataRead = await self._read(address)
            return DataRead
            return 0
        except ValueError as e:
            self.log.error("exception ValueError: %s" % (e))
            raise TestError
        except ReadTransactionException as e:
            self.log.error("exception ReadTransactionException: %s" % (e))
            raise TestError

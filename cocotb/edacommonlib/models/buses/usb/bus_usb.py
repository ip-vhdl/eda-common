"""
Bla.
"""

import cocotb
from cocotb.binary import BinaryValue
from cocotb.drivers import BusDriver
from cocotb.triggers import RisingEdge, Lock, Timer, FallingEdge, with_timeout, Lock
from cocotb.result import TestError


class WriteTransactionException(Exception):
    """
    Custom exception signaling write transactions issues.
    """

    def __init__(self, message):
        self.message = message


class ReadTransactionException(Exception):
    """
    Custom exception signaling read transactions issues.
    """

    def __init__(self, message):
        self.message = message


# * USB bus class

class usb(BusDriver):
    """On-chip peripheral bus master."""

    _signals = ["oe", "rd", "wr", "txe", "rxf", "data_in", "data_out"]
    _optional_signals = []

    def __init__(self, entity, name, clock, **kwargs):

        BusDriver.__init__(self, entity, name, clock, **kwargs)

        self.bus.rxf <= 1
        self.bus.txe <= 1
        self.bus.data_in <= BinaryValue('zzzzzzzz')
        self._lock = Lock()
        self.log.debug("usb: bus created")

    # ** Low level write

    async def _write(self, data, sync=True, trailing_ff=True):
        """
        Implements a low level usb write transaction towards the capture logic.
        """

        # Apply values for next clock edge
        if sync:
            await RisingEdge(self.clock)

        ParamsIndex = 0
        self.bus.rxf <= 0
        await FallingEdge(self.bus.oe)
        while self.bus.oe.value == 0:
            await FallingEdge(self.bus.rd)
            while (self.bus.rd.value == 0) & (ParamsIndex < len(data)):
                await FallingEdge(self.clock)
                self.bus.data_in <= data[ParamsIndex]
                self.log.debug("usb: sent byte n.{:d}, its value is 0x{:02x}".
                               format(ParamsIndex, data[ParamsIndex]))
                ParamsIndex += 1
            if trailing_ff:
                # append trainling 0xFF to the command
                await FallingEdge(self.clock)
                self.bus.data_in <= 0xFF
            await FallingEdge(self.clock)
            self.bus.rxf <= 1
            self.log.debug("usb: sent last byte n.{:d}, its value is 0x{:02x}".
                           format(ParamsIndex, 0xFF))
            await Timer(20, "ns")
            self.bus.data_in <= BinaryValue('zzzzzzzz')

        if (ParamsIndex) != len(data):
            raise WriteTransactionException("wr transaction: wrong number of params sent")

    # ** Low level read

    async def _read(self, nb_bytes, sync=True):
        """
        Implements a low level usb NB_BYTES read transaction towards the capture
        logic.
        """

        # Apply values for next clock edge
        if sync:
            await RisingEdge(self.clock)

        nb_bytes_rd = 0
        data = [0 for _ in range(nb_bytes)]  # captured data
        # loop over nb of requested bytes
        while nb_bytes_rd < nb_bytes:
            # init read transaction
            self.bus.txe <= 0
            # wait for acknowledge
            await FallingEdge(self.bus.wr)
            # capture data in falling edges
            await FallingEdge(self.clock)
            # usb controller sends burst of packets (see max_loop in fw)
            while self.bus.wr.value == 0:
                data[nb_bytes_rd] = int(self.bus.data_out)
                self.log.debug("usb: read byte n.{:d}, its value is 0x{:02x}".
                               format(nb_bytes_rd, data[nb_bytes_rd]))
                nb_bytes_rd += 1
                await FallingEdge(self.clock)
            # stop capturing data, iterate if necessary
            self.bus.txe <= 1
            await FallingEdge(self.clock)

        if nb_bytes_rd != nb_bytes:
            raise ReadTransactionException("rd transaction: wrong number of params read")
        # readout captured data
        return data

    # ** Write command

    async def write(self, order, params):
        """
        Sends and ORDER byte, followed by its array of bytes PARAMS.
        Transfert is executed in one single burst.
        """

        self.log.debug("usb: write requested, waiting for bus lock.")
        await self._lock.acquire()  # acquire lock
        self.log.debug("usb: write requested, acquired lock.")

        try:
            params.insert(0, order)  # put order in front of params
            await self._write(params)
        except ValueError as e:
            self.log.error("exception ValueError: %s" % (e))
            raise TestError
        except WriteTransactionException as e:
            self.log.error("exception WriteTransactionException: %s" % (e))
            raise TestError
        finally:
            self._lock.release()  # release lock
            self.log.debug("usb: write, released lock.")

    async def write_2(self, order, params):
        """
        Sends and ORDER byte, followed by its array of bytes PARAMS.
        Transfert is executed in two bursts split appart by 500 ns.
        """

        self.log.debug("usb: write requested, waiting for bus lock.")
        await self._lock.acquire()  # acquire lock
        self.log.debug("usb: write requested, acquired lock.")

        try:
            params.insert(0, order)  # put order in front of params
            await self._write(params[0:round(len(params)/2)], trailing_ff=False)
            # pause transfert
            await Timer(500, "ns")
            await self._write(params[round(len(params)/2):])
        except ValueError as e:
            self.log.error("exception ValueError: %s" % (e))
            raise TestError
        except WriteTransactionException as e:
            self.log.error("exception WriteTransactionException: %s" % (e))
            raise TestError
        finally:
            self._lock.release()  # release lock
            self.log.debug("usb: write, released lock.")

    # ** Read command

    async def read(self, nb_bytes):
        """
        Implements a data request of NB_BYTES.
        """

        self.log.debug("usb: read requested, waiting for bus lock.")
        await self._lock.acquire()  # acquire lock
        self.log.debug("usb: read requested, acquired lock.")

        try:
            data = await self._read(nb_bytes)
        except ValueError as e:
            self.log.error("exception ValueError: %s" % (e))
            raise TestError
        except ReadTransactionException as e:
            self.log.error("exception ReadTransactionException: %s" % (e))
            raise TestError
        finally:
            self._lock.release()  # release lock
            self.log.debug("usb: read, released lock.")

        return data

    # @cocotb.coroutine
    # def _usb_wrong_transaction(self, order, params):
    #     """
    #     Testing ...
    #     """

    #     ParamsIndex = 0
    #     params.insert(0, order)

    #     self.dut.usb_rxf <= 0
    #     await  FallingEdge(self.dut.usb_oe)
    #     while self.dut.usb_oe.value == 0:
    #         await  FallingEdge(self.dut.usb_rd)
    #         while (self.dut.usb_rd.value == 0) & (ParamsIndex < len(params) - 5):
    #             await  FallingEdge(self.dut.clk_2232)
    #             self.dut.usb_data <= params[ParamsIndex]
    #             ParamsIndex += 1

    #     self.dut.usb_rxf <= 1
    #     await  Timer(1, "us")

    #     self.dut.usb_rxf <= 0
    #     while self.dut.usb_oe.value == 0:
    #         await  FallingEdge(self.dut.usb_rd)
    #         while (self.dut.usb_rd.value == 0) & (ParamsIndex < len(params)):
    #             await  FallingEdge(self.dut.clk_2232)
    #             self.dut.usb_data <= params[ParamsIndex]
    #             ParamsIndex += 1

    #         # append trainling 0xFF to the command
    #         await  FallingEdge(self.dut.clk_2232)
    #         self.dut.usb_data <= 0xFF
    #         await  FallingEdge(self.dut.clk_2232)
    #         self.dut.usb_rxf <= 1
    #         await  Timer(100, "ns")
    #     # return correctly sent params
    #     return ParamsIndex

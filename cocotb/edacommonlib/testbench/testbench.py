'''
Base class for all testbenchs.

Provides common functionality to testbenches.

- tb_base_reset(duration, units)

  Assert high a DUT.RST signal for a DURATION length in UNITS

- tb_base_message(message, level)

  Issue a MESSAGE, including current sim time, with logging LEVEL


'''

import logging
import os
import cocotb
from cocotb.triggers import Timer
from cocotb.utils import get_sim_time


class tb_base(object):
    """
    Base class for all cocotb testbenches.
    """

    def __init__(self, dut):

        self.dut = dut

        # Set DUT logging level
        self._logging_levels = logging._levelToName  # get dict of logging levels
        try:
            index = list(self._logging_levels.values()).index(os.environ['LOGGING'])
            loglevel = list(self._logging_levels.keys())[index]
            self.tb_base_message("TB Log level set to {:s}".
                                 format(list(self._logging_levels.values())[index]),
                                 'info')
            dut._log.setLevel(loglevel)
        except ValueError:
            self.tb_base_message("LOGGING value %s not allowed {:s}".
                                 format((os.environ['LOGGING'])),
                                 'info')

    async def tb_base_reset(self, duration=1, units='us'):
        """
        Initial reset of DURATION us length.
        """

        self.tb_base_message("Starting reset", 'info')
        self.dut.rst <= 1
        await Timer(duration, units='us')
        self.dut.rst <= 0
        self.tb_base_message("End reset", 'info')

        return "Reset lenght is {} us".format(duration)

    async def tb_base_debug(self):
        """
        Start an ipython shell in the current coroutine.

        Use: put a line
             await self.tb_base_debug()
        in the code
        """

        # See [[https://docs.cocotb.org/en/latest/troubleshooting.html?highlight=ipython#module-cocotb.ipython_support][Embedding an IPython shell]]

        if self._logging_levels[self.dut._log.level] == 'DEBUG':
            from cocotb.ipython_support import embed
            # await embed(user_ns=dict(dut=self.dut))
            # await embed(user_ns=dict(dut=self))
            await embed(user_ns=locals())

    def tb_base_message(self, message, level='warning'):
        """
        Issue a MESSAGE, including current sim time, with logging LEVEL.
        """
        if level == 'warning':
            self.dut._log.warning("{:s}.\t\tTime {:2.1f} ns ({:2.1f} us)".
                                  format(message, get_sim_time('ns'), get_sim_time('us')))
        elif level == "debug":
            self.dut._log.debug("{:s}.\t\tTime {:2.1f} ns ({:2.1f} us)".
                                format(message, get_sim_time('ns'), get_sim_time('us')))
        elif level == "info":
            self.dut._log.info("{:s}.\t\tTime {:2.1f} ns ({:2.1f} us)".
                               format(message, get_sim_time('ns'), get_sim_time('us')))
        elif level == "error":
            self.dut._log.error("{:s}.\t\tTime {:2.1f} ns ({:2.1f} us)".
                                format(message, get_sim_time('ns'), get_sim_time('us')))
        elif level == "critical":
            self.dut._log.critical("{:s}.\t\tTime {:2.1f} ns ({:2.1f} us)".
                                   format(message, get_sim_time('ns'), get_sim_time('us')))

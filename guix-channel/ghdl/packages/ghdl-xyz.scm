(define-module (ghdl packages ghdl-xyz)
  #:use-module (guix)
  #:use-module (guix i18n)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (guix build-system python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-build))

(define-public python-cocotb
  (package
    (name "python-cocotb")
    (version "1.6.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "cocotb" version))
       (sha256
	(base32 "0jxxw2avvclcz7kwqqji01vz8lzx4h7izri9x0h66vf3dvpvb3s9"))))
    (build-system python-build-system)
    (arguments
     '(#:phases (modify-phases %standard-phases
		  (delete 'check))))
    (home-page "https://docs.cocotb.org")
    (synopsis
     "cocotb is a coroutine based cosimulation library for writing VHDL and Verilog testbenches in Python.")
    (description
     "cocotb is a coroutine based cosimulation library for writing VHDL and Verilog
testbenches in Python.")
    (license license:bsd-3)))

(define-public gnat6
  (package
   (name "gnat6")
   (version "0.1")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   ;; (url "https://github.com/ghdl/ghdl.git")
	   (url "https://gitlab.com/csantosb/gnat.git")
	   ;; (commit "d07caf380c45ae5e0b8e4f1d1e14e824401b6162")
	   (commit "0a39a7211615c760a82d561f77cc6a2859127eac")))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "0id110ijvb5kj4aa8l2gv9b4j1jal8rbq3nw0q4iibfx6hzbx08c"))))
   (build-system gnu-build-system)
   (arguments
    '(#:phases (modify-phases %standard-phases
			      (delete 'bootstrap)
			      (delete 'configure)
			      (delete 'build)
			      (delete 'check)
			      (delete 'install)
			      (delete 'reset-gzip-timestamps)
			      (delete 'install-license-files)
			      (add-before 'patch-shebangs 'install
					  (lambda* (#:key outputs #:allow-other-keys)
					    ;; Modify the makefile so that its
					    ;; 'PREFIX' variable points to "out".
					    (let ((out (assoc-ref outputs "out")))
					      ;; (substitute* "Makefile"
					      ;;		   (("PREFIX=.*")
					      ;;		    (string-append "PREFIX="
					      ;;				   out "\n")))
					      (substitute* "Makefile"
							   (("PREFIX=.*")
							    (string-append "PREFIX="
									   out "\n")))
					      #true)))
			      )))
   ;; (arguments
   ;;  '(#:phases (modify-phases %standard-phases
   ;;			      (delete 'configure)
   ;;			      (delete 'build)
   ;;			      (delete 'check)

   ;;			      )))
   ;; (arguments '(#:configure-flags '("--with-llvm-config")))
   ;; (arguments
   ;;  (substitute-keyword-arguments (package-arguments ghdl)
   ;;				  ((#:configure-flags '())
   ;;				   ;; `(cons* "--with-llvm-config --prefix=/usr" ,flags)
   ;;				   ;; `(append '("--with-llvm-config")
   ;;				   ;;          (remove (cut string-match "CONFIG_SHELL.*" <>)
   ;;				   ;;                  ,flags))
   ;;				   nil
   ;;				   ))
   ;;  )
   (native-inputs
    (list clang-toolchain))
   (home-page "https://github.com/ghdl/ghdl")
   (synopsis
    "Open-source analyzer, compiler, simulator and (experimental) synthesizer for VHDL.")
   (description
    "This package provides ...")
   (license #f)))

(define-public ghdl3
  (package
   (name "ghdl3")
   (version "0.1")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
	   ;; (url "https://github.com/ghdl/ghdl.git")
	   (url "https://github.com/csantosb/ghdl.git")
	   ;; (commit "d07caf380c45ae5e0b8e4f1d1e14e824401b6162")
	   (commit "59ad38cca29e6a600e16f896c57ae7af84f35589")))
     (file-name (git-file-name name version))
     (sha256
      (base32
       ;; "0hfq6g7c6yry668p6qami0sj6f7k3254b5w6b4rbfap0ns27dd2w"
       "1bxkkv7mjm3cg1dlkw2yk7fyqzwnxjy7mc70a7n6x34n2x2drsns"))))
   (build-system gnu-build-system)
   (arguments
    '(#:phases (modify-phases %standard-phases
			      (delete 'configure)
			      (add-before 'build 'configure
					  (lambda* (#:key outputs #:allow-other-keys)
					    ;; Modify the makefile so that its
					    ;; 'PREFIX' variable points to "out".
					    (let ((out (assoc-ref outputs "out")))
					      (invoke "./configure  --with-llvm-config"
						      (string-append "--prefix=" out)))
					    ;; (let ((out (assoc-ref outputs "out")))
					    ;;   (substitute* "Makefile"
					    ;;		   (("PREFIX =.*")
					    ;;		    (string-append "PREFIX = "
					    ;;				   out "\n")))
					    ;;   #true)
					    )))))
   ;; (arguments '(#:configure-flags '("--with-llvm-config")))
   ;; (arguments
   ;;  (substitute-keyword-arguments (package-arguments ghdl)
   ;;				  ((#:configure-flags '())
   ;;				   ;; `(cons* "--with-llvm-config --prefix=/usr" ,flags)
   ;;				   ;; `(append '("--with-llvm-config")
   ;;				   ;;          (remove (cut string-match "CONFIG_SHELL.*" <>)
   ;;				   ;;                  ,flags))
   ;;				   nil
   ;;				   ))
   ;;  )
   (native-inputs
    (list clang-toolchain))
   (home-page "https://github.com/ghdl/ghdl")
   (synopsis
    "Open-source analyzer, compiler, simulator and (experimental) synthesizer for VHDL.")
   (description
    "This package provides ...")
   (license #f)))

;; (define-public python-pyfftw
;;   (package
;;    (name "python-pyfftw")
;;    (version "0.13.0")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "pyFFTW" version))
;;      (sha256
;;       (base32 "1njk89w77f4l128c615jdi0dha873aq9k7migvarbgf00lj111fs"))))
;;    (build-system python-build-system)
;;    (propagated-inputs (list python-numpy))
;;    (home-page "https://github.com/pyFFTW/pyFFTW")
;;    (synopsis
;;     "A pythonic wrapper around FFTW, the FFT library, presenting a unified interface for all the supported transforms.")
;;    (description
;;     "This package provides a pythonic wrapper around FFTW, the FFT library,
;; presenting a unified interface for all the supported transforms.")
;;    (license #f)))

;; (define-public python-ptemcee
;;   (package
;;    (name "python-ptemcee")
;;    (version "1.0.0")
;;    (source
;;     (origin
;;      (method git-fetch)
;;      (uri (git-reference
;;	   (url "https://github.com/SylvainMarsat/ptemcee.git")
;;	   (commit "665da0f")))
;;      (file-name (git-file-name name version))
;;      (sha256
;;       (base32
;;        "025v85vqv9h2dd7zdi5rj6rcq1al7k81bajrq1wcdwmmkrq1gz0h"))))
;;    (build-system python-build-system)
;;    (native-inputs
;;     (list python-numpy python-attrs python-pytest python-pytest-xdist))
;;    (home-page "https://github.com/SylvainMarsat/ptemcee.git")
;;    (synopsis
;;     "Adaptive parallel tempering meets emcee.")
;;    (description
;;     "ptemcee, is fork of Daniel Foreman-Mackey's emcee to implement parallel tempering more robustly. As far as possible, it is designed as a drop-in replacement for emcee.")
;;    (license #f)))

;; (define-public python-corner
;;   (package
;;    (name "python-corner")
;;    (version "2.2.1")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "corner" version))
;;      (sha256
;;       (base32 "1rai8yk4b9zw644szi7l189xf6p5fcsv503dl0mbp4qhsfd1v7yk"))))
;;    ;; (source
;;    ;;  (origin
;;    ;;    (method git-fetch)
;;    ;;    (uri (git-reference
;;    ;;	     (url "https://github.com/dfm/corner.py.git")
;;    ;;	     (commit (string-append "v" version))))
;;    ;;    (file-name (git-file-name name version))
;;    ;;    (sha256
;;    ;;	(base32 "1f61gk7lbxzc612a8rrc8i152pyzjzlq2iwgsc9lsvxzppl7a64x"))))
;;    (build-system python-build-system)
;;    (propagated-inputs (list python-matplotlib))
;;    (native-inputs
;;     (list
;;      ;; python-arviz
;;      python-black
;;      python-flake8
;;      python-isort
;;      ;; python-myst-nb
;;      python-pypandoc
;;      python-pep517
;;      python-pre-commit
;;      python-pytest
;;      python-pytest-cov
;;      python-sphinx
;;      ;; python-sphinx-book-theme
;;      python-toml
;;      python-twine))
;;    (home-page "https://corner.readthedocs.io")
;;    (synopsis "Make some beautiful corner plots")
;;    (description "Make some beautiful corner plots")
;;    (license #f)))

;; (define-public python-bounded-pool-executor
;;   (package
;;    (name "python-bounded-pool-executor")
;;    (version "0.0.3")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "bounded_pool_executor" version))
;;      (sha256
;;       (base32 "0vkzc94ib5ndsffqwvab9aihyn00v2g1z0v421g5bplaqcdj54p0"))))
;;    (build-system python-build-system)
;;    (home-page "http://github.com/mowshon/bounded_pool_executor")
;;    (synopsis "Bounded Process&Thread Pool Executor")
;;    (description "Bounded Process&Thread Pool Executor")
;;    (license #f)))

;; (define-public python-pqdm
;;   (package
;;    (name "python-pqdm")
;;    (version "0.1.0")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "pqdm" version))
;;      (sha256
;;       (base32 "1h636n5rsixqdwnyqgmgxg95jl44wlx2n8913l4jj480k63mnh2v"))))
;;    (build-system python-build-system)
;;    (propagated-inputs
;;     (list python-bounded-pool-executor python-tqdm python-typing-extensions))
;;    (native-inputs
;;     (list python-pytest-runner python-pytest))
;;    (home-page "https://github.com/niedakh/pqdm")
;;    (synopsis
;;     "PQDM is a TQDM and concurrent futures wrapper to allow enjoyable paralellization of progress bars.")
;;    (description
;;     "PQDM is a TQDM and concurrent futures wrapper to allow enjoyable paralellization
;; of progress bars.")
;;    (license #f)))

;; (define-public python-pyrxp
;;   (package
;;    (name "python-pyrxp")
;;    (version "3.0.1")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "pyRXP" version))
;;      (sha256
;;       (base32 "16my8c8gbwr7zgg125p9h9m4sl3bg37zral00f29ycig6kp3smpm"))))
;;    (build-system python-build-system)
;;    (home-page "http://www.reportlab.com")
;;    (synopsis "Python RXP interface - fast validating XML parser")
;;    (description "Python RXP interface - fast validating XML parser")
;;    (license #f)))

;; (define-public python-gwaxion
;;   (package
;;    (name "python-gwaxion")
;;    (version "0.0.1")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "gwaxion" version))
;;      (sha256
;;       (base32 "1kdfii6flhawv5ik6bgzz3kagb86g1mksyz30r7p6xizvxx9n11q"))))
;;    (build-system python-build-system)
;;    (propagated-inputs (list python-scipy))
;;    (native-inputs (list python-recommonmark python-numexpr python-scipy python-corner python-twine
;;			python-astropy python-h5py python-matplotlib python-pandas python-wheel))
;;    (home-page "https://github.com/maxisi/gwaxion")
;;    (synopsis "Black-hole boson gravitational-wave package.")
;;    (description "Black-hole boson gravitational-wave package.")
;;    (license #f)))

;; (define-public python-emcee
;;   (package
;;    (name "python-emcee")
;;    (version "3.1.1")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "emcee" version))
;;      (sha256
;;       (base32 "0p7v2094yy82gfbmpd41xxcrjsi8qy26219nm2vn05y5ynkwdzs8"))))
;;    (build-system python-build-system)
;;    (propagated-inputs (list python-numpy))
;;    (native-inputs (list python-coverage python-pytest python-pytest-cov python-wheel python-setuptools python-setuptools-scm python-scipy python-h5py))
;;    (home-page "https://emcee.readthedocs.io")
;;    (synopsis "The Python ensemble sampling toolkit for MCMC")
;;    (description "The Python ensemble sampling toolkit for MCMC")
;;    (license #f)))

;; (define-public python-vegas
;;   (package
;;    (name "python-vegas")
;;    (version "5.1.1")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "vegas" version))
;;      (sha256
;;       (base32 "0g382k7inc5sx4w5qz0wiyzqyzrcxfhw52zm6p8yirshrknh0vp0"))))
;;    (build-system python-build-system)
;;    (propagated-inputs (list python-gvar python-numpy python-cython))
;;    (home-page "https://github.com/gplepage/vegas.git")
;;    (synopsis "Tools for adaptive multidimensional Monte Carlo integration.")
;;    (description "Tools for adaptive multidimensional Monte Carlo integration.")
;;    (license #f)))

;; (define-public python-gvar
;;   (package
;;    (name "python-gvar")
;;    (version "11.9.5")
;;    (source
;;     (origin
;;      (method url-fetch)
;;      (uri (pypi-uri "gvar" version))
;;      (sha256
;;       (base32 "1v0v6736cm8rw76bym6zwrrxwb68nzg8agb0b5g1633pxq8a8rij"))))
;;    (build-system python-build-system)
;;    (propagated-inputs (list python-numpy python-scipy python-cython))
;;    (home-page "https://github.com/gplepage/gvar.git")
;;    (synopsis "Utilities for manipulating correlated Gaussian random variables.")
;;    (description
;;     "Utilities for manipulating correlated Gaussian random variables.")
;;    (license #f)))

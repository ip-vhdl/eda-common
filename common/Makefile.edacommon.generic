# * About

# This makefile is intended to be used as a common one for all cocotb/ghdl
# projects.

# Its purpose is to first define default values and then to derive more complex
# variables from the user defined ones.

# It is expected to be included from the simulation dedicated makefile, which
# will comprise all project related parameters, specific to the simulation at
# hand. Here only global, common setup must appear.

# Refer to the project wiki for more information

#       https://gitlab.com/ip-vhdl/eda-common/-/wikis/home

# * Check required

ifeq ($(EDAMODE),)
$(error "EDAMODE empty. Please set it to one of cocotb / ghdl.")
endif

# * Common variables: defaults

# Topmost level module name
TOPLEVEL ?= top

# Flag for logging messages: one of DEBUG, INFO, WARNING, ERROR, CRITICAL
#
# - DEBUG	: show tb debug messages
# - INFO	: show tb info messages
# - WARNING	: show make messages and tb warning messages
# - ERROR	: show make messages and tb error messages
# - CRITICAL	: show make messages and tb critical messages

# exporting makes the variable available to cocotb tb
export LOGGING ?= WARNING

ifeq ($(LOGGING),$(filter $(LOGGING),INFO DEBUG))
$(info )$(info "info: EDA_COMMON_DIR is $(EDA_COMMON_DIR)")$(info )
$(info )$(info "info: Including $(EDA_COMMON_DIR)/common/Makefile.edacommon.generic")$(info )
endif

# ** Sources

# *** Under current project

# Directories where project sources are to be found
PRJ_SRC_DIR ?= {Fuentes,src}

# Sparse source rtl code
PRJ_SRC_FILES ?=

# *** Not under current project

# Directory with ancillary sources
NO_PRJ_ROOTDIR ?=

# Directories where non project sources are to be found
NO_PRJ_SRC_DIR ?=

# Sparse source rtl code
NO_PRJ_SRC_FILES ?=

# ** Ghdl

# Kind of wave file to save results to: one of ghw/vcd
GHDL_WAVE_TYPE ?=

# Duration of simulation
GHDL_SIM_TIME ?=

# Vivado, ISE
GHDL_XILINX_TOOL ?= Vivado

# 08, 93c
GHDL_STD ?= 08

 # synopsys, standard
GHDL_IEEE ?= standard

# Ghdl sim flags
# To be used during run stage ("ghdl -r")
# Note the use of override ; https://www.gnu.org/software/make/manual/make.html#Override-Directive
# ifneq ($(SIM_ARGS),)
#	override SIM_ARGS += --wave=$(MODULE)_wave.ghw --ieee-asserts=disable
# else
#	override SIM_ARGS := --wave=$(MODULE)_wave.ghw --ieee-asserts=disable
# endif
GHDL_RUNFLAGS ?= --ieee-asserts=enable

# Include ghdl coverage analysis flags
GHDL_COVERAGE ?=

SIM_BUILD ?= sim_build

# ** Cocotb

ifeq ($(EDAMODE),cocotb)
ifeq ($(COCOTB_TB),)
$(info  "info: COCOTB_TB variable not set, please fix it.")
$(info )
$(error "COCOTB_TB empty.")
endif
endif

# * Derived variables

# ** Ghdl

# Rtl work lib
GHDL_WORK = work

# Wave file type
ifeq ($(GHDL_WAVE_TYPE),)
GHDL_WAVE_FILE =
ifeq ($(LOGGING),$(filter $(LOGGING),INFO DEBUG))
$(info "info: No wave file is recorded.")$(info )
endif
else ifeq ($(GHDL_WAVE_TYPE),ghw)
GHDL_WAVE_FILE = wave.ghw
ifeq ($(LOGGING),$(filter $(LOGGING),INFO DEBUG))
$(info "info: Wave file is $(GHDL_WAVE_FILE)")$(info )
endif
else ifeq ($(GHDL_WAVE_TYPE),vcd)
GHDL_WAVE_FILE = wave.vcd
ifeq ($(LOGGING),$(filter $(LOGGING),INFO DEBUG))
$(info "info: Wave file is $(GHDL_WAVE_FILE)")$(info )
endif
endif

# lib to include sources
GHDL_LIB = $(GHDL_WORK)-obj$(GHDL_STD).cf
ifeq ($(LOGGING),$(filter $(LOGGING),INFO DEBUG))
$(info "info: Using GHDL_LIB $(GHDL_LIB)") $(info )
endif

# *** ghdl runflags

# runtime flags

ifeq ($(GHDL_WAVE_TYPE),)
GHDL_RUNFLAGS := $(GHDL_RUNFLAGS)
else ifeq ($(GHDL_WAVE_TYPE),ghw)
GHDL_RUNFLAGS := $(GHDL_RUNFLAGS) --wave=$(GHDL_WAVE_FILE)
else ifeq ($(GHDL_WAVE_TYPE),vcd)
GHDL_RUNFLAGS := $(GHDL_RUNFLAGS) --vcd=$(GHDL_WAVE_FILE)
endif

ifneq ($(GHDL_SIM_TIME),)
GHDL_RUNFLAGS := $(GHDL_RUNFLAGS) --stop-time=$(GHDL_SIM_TIME)
endif

ifeq ($(LOGGING),$(filter $(LOGGING),INFO DEBUG))
$(info "info: Using GHDL_RUNFLAGS $(GHDL_RUNFLAGS)") $(info )
endif

# *** ghdl options

# general options, used during include and build

GHDL_OPTIONS = --std=$(GHDL_STD) --ieee=$(GHDL_IEEE) --mb-comments -fpsl -C

ifneq ($(GHDL_XILINX_COMPILED_LIBRARIES),)
GHDL_OPTIONS := $(GHDL_OPTIONS) -P$(GHDL_XILINX_COMPILED_LIBRARIES)/$(GHDL_XILINX_TOOL)
endif

ifneq ($(GHDL_OSVVM_COMPILED_LIBRARIES),)
GHDL_OPTIONS := $(GHDL_OPTIONS) -P$(GHDL_OSVVM_COMPILED_LIBRARIES)
endif

ifneq ($(GHDL_OSVVM_VERIFICATIONIP_COMPILED_LIBRARIES),)
GHDL_OPTIONS := $(GHDL_OPTIONS) -P$(GHDL_OSVVM_VERIFICATIONIP_COMPILED_LIBRARIES)/osvvm_common -P$(GHDL_OSVVM_VERIFICATIONIP_COMPILED_LIBRARIES)/osvvm_uart -P$(GHDL_OSVVM_VERIFICATIONIP_COMPILED_LIBRARIES)/osvvm_axi4
endif

# optionally add this flag when not using standard packages
# see [[https://ghdl.readthedocs.io/en/latest/using/InvokingGHDL.html#ieee-library-pitfalls][IEEE library pitfalls]], [[https://ghdl.readthedocs.io/en/latest/using/InvokingGHDL.html#ghdl-options][ghdl options]] and [[https://ghdl.readthedocs.io/en/latest/using/ImplementationOfVHDL.html#using-vendor-libraries][using vendor libraries]]

ifneq ($(GHDL_IEEE),standard)
GHDL_OPTIONS := $(GHDL_OPTIONS) -fexplicit -frelaxed --no-vital-checks
ifeq ($(LOGGING),$(filter $(LOGGING),INFO DEBUG))
$(info "info: Setting relaxing flags in GHDL_OPTIONS") $(info )
endif
endif

# add these warnings, not by default; see [[https://ghdl.readthedocs.io/en/latest/using/InvokingGHDL.html#warnings][ghdl warnings]]

GHDL_OPTIONS := $(GHDL_OPTIONS) --warn-unused --warn-reserved --warn-parenthesis --warn-delayed-checks --warn-others --warn-pure --warn-static

# split appart include (-i) and make (-m) phases, so that passing different
# flags is possible (useful with coverage, for example)

# **** include

# To be used during include stage ("ghdl -i")

GHDL_OPTIONS_INCLUDE = $(GHDL_OPTIONS) -O2  # -g

# **** make

# To be used during compile stage ("ghdl -m")

ifneq ($(GHDL_COVERAGE),)
# optionally add coverage flags; see [[https://stackoverflow.com/questions/38852986/ghdl-code-coverage-using-gcov-ubuntu-16-04-lts][this]] and [[https://devsaurus.github.io/ghdl_gcov/ghdl_gcov.html][this]] references
GHDL_OPTIONS_MAKE := $(GHDL_OPTIONS) -O0 -Wc,-ftest-coverage -Wc,-fprofile-arcs -Wl,-lgcov -Wl,--coverage
ifeq ($(LOGGING),$(filter $(LOGGING),INFO DEBUG))
$(info "info: Setting coverage analysis flags in GHDL_OPTIONS_MAKE") $(info )
endif
else
# as for [[https://gcc.gnu.org/onlinedocs/gcc/Gcov-Intro.html#Gcov-Intro][this]], don’t optimize code when doing coverage
GHDL_OPTIONS_MAKE := $(GHDL_OPTIONS) -O2 # -g
endif

ifeq ($(LOGGING),$(filter $(LOGGING),INFO DEBUG))
$(info "info: Using GHDL_OPTIONS_INCLUDE: $(GHDL_OPTIONS_INCLUDE)") $(info )
$(info "info: Using GHDL_OPTIONS_MAKE: $(GHDL_OPTIONS_MAKE)") $(info )
endif

# ** General

# Project root dir

# Display project root directory, just to be sure we’re doing it right
ifeq ($(PRJ_ROOTDIR),)
PRJ_ROOTDIR := $(strip $(shell git rev-parse --show-toplevel))
endif
ifeq ($(LOGGING),$(filter $(LOGGING),INFO DEBUG))
$(info "info: Using this project root $(PRJ_ROOTDIR)") $(info )
endif

# ** Sources

# List of source code files

# Array of sources: consider vhdl files too
# convert array of directories to an array of source rtl code files
ifneq ($(PRJ_ROOTDIR),)
ifneq ($(PRJ_SRC_DIR),)
# i. longer, more complex
# VHDL_SOURCES := $(strip $(shell find $(PRJ_ROOTDIR)/$(PRJ_SRC_DIR) -not -path '*/\.*' -type f -iname *.vhd* | sed /backups/d))
# ii. doesn’t do it
# VHDL_SOURCES := $(wildcard $(PRJ_ROOTDIR)/$(PRJ_SRC_DIR)/*.vhd)
# iii. Get sources from project file
# VHDL_SOURCES := $(strip $(shell cat $(PRJ_FILE) | grep -oE '[^[:blank:]]+[[:blank:]]*$$' | tr -d '"' ))
# iv. simpler and efficient !
# TODO: replace this, removing the grep part, and using an expression taking only .vhd and .vhdl files
VHDL_SOURCES += $(strip $(shell find $(PRJ_ROOTDIR)/$(PRJ_SRC_DIR) -type f \( -iname \*.vhd\* \) | grep -v .backups ))
endif
endif

ifneq ($(PRJ_ROOTDIR),)
ifneq ($(PRJ_SRC_FILES),)
VHDL_SOURCES += $(strip $(shell find $(PRJ_ROOTDIR)/$(PRJ_SRC_FILES) -type f))
endif
endif

# See case of PRJ_SRC_DIR above for alternatives
ifneq ($(NO_PRJ_ROOTDIR),)
ifneq ($(NO_PRJ_SRC_DIR),)
VHDL_SOURCES += $(strip $(shell find $(NO_PRJ_ROOTDIR)/$(NO_PRJ_SRC_DIR) -type f \( -iname \*.vhd\* \) | grep -v .backups ))
endif
endif

ifneq ($(NO_PRJ_ROOTDIR),)
ifneq ($(NO_PRJ_SRC_FILES),)
VHDL_SOURCES += $(strip $(shell find $(NO_PRJ_ROOTDIR)/$(NO_PRJ_SRC_FILES) -type f))
endif
endif

ifeq ($(LOGGING),$(filter $(LOGGING),INFO DEBUG))
$(info "info: VHDL_SOURCES = ${VHDL_SOURCES}") $(info )
endif

# Another way of producing the VHDL_SOURCES array would be the following

# # Filelist with all vhdl files in project
# FILELIST := $(shell hogit rev-parse --show-toplevel)/gtags.files
# $(info $$FileList is ${FILELIST})
# $(info )

# Project root directory, replacing all occurrences of / by a \\/ (necessary in next step)
# PRJ_ROOTDIR := $(strip $(shell git rev-parse --show-toplevel | sed s/\\//\\\\\\\\\\//g ))
# $(info "info: PRJ_ROOTDIR=$(PRJ_ROOTDIR)") $(info )

# # Array of sources
# VHDL_SOURCES := $(strip $(shell cat $(FILELIST) | sed s/^\\./$(PRJ_ROOTDIR)/g))
# $(info $$SOURCES is ${SOURCES})
# $(info )

# * Include specific makefile following the workflow in use

# ghdl case
ifeq ($(EDAMODE),ghdl)
ifeq ($(LOGGING),$(filter $(LOGGING),INFO DEBUG))
$(info "info: Including $(EDA_COMMON_DIR)/ghdl/Makefile.edacommon.ghdl")
endif
-include $(EDA_COMMON_DIR)/ghdl/Makefile.edacommon.ghdl
endif

# cocotb case
ifeq ($(EDAMODE),cocotb)
ifeq ($(LOGGING),$(filter $(LOGGING),INFO DEBUG))
$(info "info: Including $(EDA_COMMON_DIR)/cocotb/Makefile.edacommon.cocotb")
endif
-include $(EDA_COMMON_DIR)/cocotb/Makefile.edacommon.cocotb
endif

# * About

# This makefile is intended to be used as a common base for ghdl and cocotb simulations.
#
# Its most up to date version may be found at the address
#
#       https://gitlab.com/ip-vhdl/eda-common/-/blob/master/common/Makefile.edacommon

# Refer to the project wiki for more information
#
#       https://gitlab.com/ip-vhdl/eda-common/-/wikis/home

# * Use

# For its use, refer to readme.org and follow below indications. Note that an
# example use of each variable is provided too. Finally, if a variable is left
# empty, a default value is taken.

# * Required

# # Mandatory external variable
# # check if EDA_COMMON_DIR variable is defined; otherwise, stop here

# ifeq ($(EDA_COMMON_DIR),)
# $(info  "info: EDA_COMMON_DIR is empty")
# $(info  "      Please declare it as an env variable")
# $(info  "      or declare it as")
# $(info  "          make EDA_COMMON_DIR=/your_path/")
# $(info )
# $(error "EDA_COMMON_DIR empty")
# endif

# # Mandatory variable
# # Workflow: one of ghdl / cocotb
# EDAMODE =

# # Autoset variable, name of calling original Makefile
# MAKEFILENAME := $(CURDIR)/$(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST))

# # * Project variables

# # Put your own variables in here
# # PRJ_VAR = 1

# # * Generics

# # Number of input streams
# # export g_generic1 ?=

# # data bus width
# # export g_generic2 ?=

# # * Constants

# # I proceed this way instead of doing as with generics as otherwise if forces
# # recompiling the whole project under cocotb

# # ifeq ($(C_CONSTANT),)
# # else
# #   export C_CONSTANT := C_CONSTANT
# #   # Update the constant in package before launching compilation
# #   $(shell sed -i "s/constant RegBankLength.*: natural.*;/constant RegBankLength : natural := $(C_CONSTANT);/" hdl/ft2232h_tb_pkg.vhd )
# # endif

# # * Common variables

# # Topmost level module name
# # TOPLEVEL = top

# # Flag for logging messages: one of DEBUG, INFO, WARNING, ERROR, CRITICAL
# #
# # - DEBUG	: show tb debug messages
# # - INFO	: show tb info messages
# # - WARNING	: show make messages and tb warning messages
# # - ERROR	: show make messages and tb error messages
# # - CRITICAL	: show make messages and tb critical messages

# # LOGGING = WARNING

# # ** Sources

# # *** Under current project

# # When PRJ_ROOTDIR is left empty, it is deduced from git root dir
# # So when under a git repo, better avoit setting PRJ_ROOTDIR
# #
# # Otherwise, when out of a git repo, you may use something like
# # ifneq ($(shell git rev-parse 2>&1 && echo true),true)
# # PRJ_ROOTDIR = /tmp/prj_rootdir
# # endif

# # Directories where project sources are to be found
# # PRJ_SRC_DIR = {Fuentes,src}

# # For example, use this format in case of more than one
# #
# # PRJ_SRC_DIR = {sim/runs/mux_fifo.cocotb/hdl,src/ip}
# #
# # Use this format in case of one source
# #
# # PRJ_SRC_DIR = src

# # Sparse source rtl code
# # PRJ_SRC_FILES =

# # For example, use this format in case of more than one
# #
# # PRJ_SRC_FILES = {ip/fifo_sim_netlist.vhd,ip/mux_fifo_fifo_out_sim_netlist.vhd}
# #
# # Use this format in case of one source
# #
# # PRJ_SRC_FILES = fifo.vhd

# # *** Not under current project

# # Same indications as above apply

# # Directory with ancillary sources
# # NO_PRJ_ROOTDIR =

# # Directories where non project sources are to be found
# # NO_PRJ_SRC_DIR =

# # Sparse source rtl code
# # NO_PRJ_SRC_FILES =

# # ** Ghdl

# # To be used with ghdl

# # Kind of wave file to save results to: one of ghw / vcd
# # When left empty, no wave is recorded
# # GHDL_WAVE_TYPE =

# # For example, following the LOGGING level
# #
# # ifeq ($(LOGGING),DEBUG)
# # GHDL_WAVE_TYPE = vcd
# # endif

# # Duration of simulation
# # GHDL_SIM_TIME =

# # For example, 200 us
# #
# # GHDL_SIM_TIME = 200us

# # Xilinx libraries to be used: one of Vivado / ISE
# # GHDL_XILINX_TOOL = Vivado

# # VHDL standard: one of 08 / 93c
# # GHDL_STD = 08

#  # IEEE libraries : one of synopsys / standard
# # GHDL_IEEE = standard

# # Simulation flags
# # GHDL_RUNFLAGS = --ieee-asserts=enable

# # For example
# #
# # GHDL_RUNFLAGS = --ieee-asserts=enable -gg_generic1=$(g_generic1) -gg_generic2=$(g_generic2)

# # Include ghdl coverage analysis flags
# # GHDL_COVERAGE =

# # Where to find Xilinx compiled libraries
# # GHDL_XILINX_COMPILED_LIBRARIES = /opt/Xilinx/CompiledLibraries

# # Where to find Osvvm compiled libraries
# # GHDL_OSVVM_COMPILED_LIBRARIES = /opt/HDL/osvvm/CompiledLibraries/2020.05

# # Where to find Osvvm verificationip compiled libraries
# # GHDL_OSVVM_VERIFICATIONIP_COMPILED_LIBRARIES = /opt/HDL/osvvm/CompiledLibraries/

# # ** Cocotb

# # Applies when simulating with cocotb

# # Remembar that as for [[https://stackoverflow.com/questions/47475325/pass-argument-from-makefile-to-cocotb-testbench][this]] and [[https://stackoverflow.com/questions/23843106/how-to-set-child-process-environment-variable-in-makefile][that]], any variable in the make file may be made accessible to the
# # python testbench by using

# # export VARIABLENAME =

# and then using import os: os.environ[’VARIABLENAME']

# Python testbench file name
# COCOTB_TB =

# * Include global makefile

# Include the generic makefile, common to ghdl / cocotb flows

-include $(EDA_COMMON_DIR)/common/Makefile.edacommon.generic

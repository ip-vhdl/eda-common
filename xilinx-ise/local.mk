# topmost level
TOP = top

# constraints file
UCF_FILE = ../src/$(TOP).ucf

# Array of directories with rtl sources
PRJ_SRC_DIR = {/src, /src2}
# PRJ_SRC_DIR = src

MAP_OPTIONS = -w -ir off -pr b -detail -mt 2 -ol high -logic_opt off -c 100

PR_OPTIONS = -w -ol high -mt 4

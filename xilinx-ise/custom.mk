REGBANKLENGTH := 540

NB_ASICS := 8

NB_DISCRI_CH := 16

# EXECTHIS_FILE := ../src-kindex7/execthis.sh

# $(shell $(EXECTHIS_FILE) $(NB_ASICS) $(NB_DISCRI_CH) $(REGBANKLENGTH) $(TOP_FILE))

# Update the the "nb_asics" generic
$(shell sed -i.delete "s/nb_asics.*: natural.*;/nb_asics : natural range 1 to 8 := $(NB_ASICS);/" $(TOP_FILE) )

# Update the the "num_of_discri_channels" generic
$(shell sed -i.delete "s/num_of_discri_channels.*: natural.*;/num_of_discri_channels : natural range 1 to 16 := $(NB_DISCRI_CH);/" $(TOP_FILE) )

# Update the the "RegBankLength" generic
$(shell sed -i.delete "s/RegBankLength.*: natural.*;/RegBankLength : natural := $(REGBANKLENGTH);/" $(TOP_FILE) )

# Beautify the file after previous modifications
$(shell emacs $(TOP_FILE) --batch --eval="(progn (setq vhdl-basic-offset 3)(delete-trailing-whitespace)(vhdl-beautify-buffer))")

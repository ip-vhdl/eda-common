#!/bin/sh

# Create a link to

#   gitlab.com/ip-vhdl/eda-common/xilinx-ise/Makefile

# as a supporting, common file to all simulations

[ -e Makefile ] && rm Makefile
ln -s $HOME/Projects/perso/eda-common/xilinx-ise/Makefile Makefile

echo "Done."
